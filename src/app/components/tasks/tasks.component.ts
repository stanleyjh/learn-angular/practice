import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Task } from '../../Task';
import { TaskItemComponent } from '../task-item/task-item.component';
import { TaskService } from '../../services/task.service';

@Component({
  selector: 'app-tasks',
  standalone: true,
  imports: [CommonModule, TaskItemComponent],
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss'],
})
export class TasksComponent implements OnInit {
  tasks: Task[] = [];

  // (1) Pass an Angular service (TaskService) in a component's (TaskComponent) constructor.
  constructor(private taskService: TaskService) {}

  ngOnInit(): void {
    // (2) tasks is initialized with taskService.getTasks().
    this.tasks = this.taskService.getTasks();
  }
}
